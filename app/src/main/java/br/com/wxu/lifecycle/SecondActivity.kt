package br.com.wxu.lifecycle

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        Log.i("STATE_ACTIVITY", "SecondActivity - onCreate()")
    }

    override fun onStart() {
        super.onStart()
        Log.i("STATE_ACTIVITY", "SecondActivity - onStart()")
    }

    override fun onResume() {
        super.onResume()
        Log.i("STATE_ACTIVITY", "SecondActivity - onResume()")
    }

    override fun onPause() {
        super.onPause()
        Log.i("STATE_ACTIVITY", "SecondActivity - onPause()")
    }

    override fun onStop() {
        super.onStop()
        Log.i("STATE_ACTIVITY", "SecondActivity - onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("STATE_ACTIVITY", "SecondActivity - onDestroy()")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i("STATE_ACTIVITY", "SecondActivity - onRestart()")
    }
}
