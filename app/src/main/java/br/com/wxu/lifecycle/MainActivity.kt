package br.com.wxu.lifecycle

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button

class MainActivity : AppCompatActivity() , OnClickListener {
    private val button by lazy { findViewById<Button>(R.id.button) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener(this)

        Log.i("STATE_ACTIVITY", "MainActivity - onCreate()")
    }

    override fun onClick(view: View?) {
        val intent = Intent(this@MainActivity, SecondActivity::class.java)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        Log.i("STATE_ACTIVITY", "MainActivity - onStart()")
    }

    override fun onResume() {
        super.onResume()
        Log.i("STATE_ACTIVITY", "MainActivity - onResume()")
    }

    override fun onPause() {
        super.onPause()
        Log.i("STATE_ACTIVITY", "MainActivity - onPause()")
    }

    override fun onStop() {
        super.onStop()
        Log.i("STATE_ACTIVITY", "MainActivity - onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("STATE_ACTIVITY", "MainActivity - onDestroy()")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i("STATE_ACTIVITY", "MainActivity - onRestart()")
    }
}
